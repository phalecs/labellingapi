FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80 443

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY . .
RUN dotnet restore

WORKDIR "/src/Label.Api"
RUN dotnet build "Label.Api.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Label.Api.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENV ASPNETCORE_URLS=http://+:80
ENTRYPOINT ["dotnet", "Label.Api.dll"]