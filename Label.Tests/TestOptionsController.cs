﻿using Label.Api.Controllers;
using Label.Api.Repository;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Label.Tests
{
  
    public class TestOptionsController
    {
        [Fact]
        public async Task GetAccessoryTypes()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetAccessoryTypes();
            Assert.Equal(16, result.Count());
        }

        [Fact]
        public async Task GetAgeGroups()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetAgeGroups();
            Assert.Equal(10, result.Count());
        }

        [Fact]
        public async Task GetBodyTypes()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetBodyTypes();
            Assert.Equal(6, result.Count());
        }

        [Fact]
        public async Task GetEventStyles()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetEventStyles();
            Assert.Equal(13, result.Count());
        }

        [Fact]
        public async Task GetFabricTypes()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetFabricTypes();
            Assert.Equal(21, result.Count());
        }

        [Fact]
        public async Task GetFabricYards()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetFabricYards();
            Assert.Equal(7, result.Count());
        }

        [Fact]
        public async Task GetFamilyTypes()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetFamilyTypes();
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public async Task GetGroupDescriptions()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetGroupDescriptions();
            Assert.Equal(4, result.Count());
        }

        [Fact]
        public async Task GetParentChildrenRelationships()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetParentChildrenRelationships();
            Assert.Equal(5, result.Count());
        }

        [Fact]
        public async Task GetPictureNumberTypes()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetPictureNumberTypes();
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public async Task GetPictureTypes()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetPictureTypes();
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public async Task GetSexes()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetSexes();
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public async Task GetStyles()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetStyles();
            Assert.Equal(13, result.Count());
        }


        [Fact]
        public async Task GetStyleType()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetStyleType();
            Assert.Equal(17, result.Count());
        }

        [Fact]
        public async Task GetStylish()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetStylish();
            Assert.Equal(4, result.Count());
        }

        [Fact]
        public async Task GetTailorCosts()
        {
            var repository = new OptionsRepository();
            var controller = new OptionsController(repository);

            var result = await controller.GetTailorCosts();
            Assert.Equal(4, result.Count());
        }
    }
}
