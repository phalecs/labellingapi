
using System;
using System.Collections.Generic;
using Label.Api;
using Label.Api.Database;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

public class TestStartup : Startup
{
    public TestStartup(IHostingEnvironment env) : base(env)
    {
    }
    
    public override void ConfigureDatabaseService(IServiceCollection services)
    {
        services.AddDbContext<LabelContext>(options => {
            options.UseInMemoryDatabase("Label");
        });
 
    }
}