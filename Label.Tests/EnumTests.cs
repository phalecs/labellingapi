using Label.Api.Logic;
using Label.Api.Models.Enums;
using System;
using System.Collections.Generic;
using Xunit;

namespace Label.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test_Enum_Without_Description()
        {
            Assert.Null(Sex.Male.GetDescription());
            Assert.Null(Sex.Female.GetDescription());

        }

        [Fact]
        public void Test_Enum_With_Description()
        {
            Assert.NotNull(PictureType.Accesories.GetDescription());
            Assert.NotNull(PictureType.Fabric.GetDescription());
            Assert.NotNull(PictureType.People_In_Attire.GetDescription());
        }

        [Fact]
        public void Test_GetEnumDictionary()
        {
            IDictionary<Int16, string> expectedResult = new Dictionary<short, string> () {
                {1, "Male"},
                {2, "Female"},
            };
            var actualResult = EnumExtension.GetEnumDictionary<Sex>();
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Test_PictureTypeGetEnumDictionary()
        {
            IDictionary<Int16, string> expectedResult = new Dictionary<short, string>() {
                {1, "Fabric"},
                {2, "Attire/ Person/People in attire"},
                {3, "Accessories" }
            };
            
            var actualResult = EnumExtension.GetEnumDictionary<PictureType>();
            Assert.Equal(expectedResult, actualResult);
            Assert.DoesNotContain(actualResult.Values, x => x.Contains("_"));
        }


    }
}
