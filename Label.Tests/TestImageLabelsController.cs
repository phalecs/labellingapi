﻿using Label.Api.Controllers;
using Label.Api.IRepository;
using Label.Api.Models;
using Label.Api.Models.Enums;
using Label.Api.Repository;
using Label.Api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Label.Tests
{
    public class TestImageLabelsController
    {
        private readonly ImageLabelsController _controller;
        private readonly string _userId;
        private readonly ImageLabel _sampleLabel;
        private readonly Mock<IImageRepository> _imageRepositoryMock;

        private readonly Mock<IUserRepository> _userRepositoryMock;

        public TestImageLabelsController()
        {
             
            _imageRepositoryMock = new Mock<IImageRepository>();
            _userRepositoryMock = new Mock<IUserRepository>();
            _controller = new ImageLabelsController(_imageRepositoryMock.Object, _userRepositoryMock.Object);
            _userId = "Sample";
             _sampleLabel = new ImageLabel()
            {
                Id=1,
                AccessoryType = AccessoryType.Bag,
                AttireSingleAgeGroup = AgeGroup.A_19_25,
                AttireSingleBodyType = BodyType.Plus_Size,
                EventStyle = EventStyle.Church,
                FabricType = FabricType.Ankara,
                AttireFabricYards = FabricYard.Three,
                AttireGroupFamily = FamilyType.Nuclear_Family,
                AttireGroupDescription = GroupDescription.Couple,
                Image = new Image() {Id = 1, Url="https://sample.com"},
                AttireGroupParentChildRelationship = ParentChildRelationship.Father_And_Son
            };
            _userRepositoryMock.Setup(x => x.GetUserId()).Returns(_userId);
            
        }
        [Fact]
        public void PostImageLabel()
        {
            var result = _controller.Post(_sampleLabel);

            var okObjectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal((int)HttpStatusCode.OK, okObjectResult.StatusCode);
            var returnValue = Assert.IsType<ImageLabel>(okObjectResult.Value);
            Assert.Equal<ImageLabel>(_sampleLabel, returnValue);
        }

        [Fact]
        public void GetNexImage_NotFound()
        {
            _imageRepositoryMock.Setup(repo => repo.GetNextLabel(_userId)).Returns((ImageLabel)null);          
            var result = _controller.Get();

            var notFoundObjectResult = Assert.IsType<NotFoundResult>(result);
            Assert.Equal((int)HttpStatusCode.NotFound, notFoundObjectResult.StatusCode);
        }

        [Fact]
        public void GetNexImage_ExistingLabel()
        {   
            _imageRepositoryMock.Setup(x => x.GetNextLabel(_userId)).Returns(_sampleLabel);
            var result = _controller.Get();

            var okObjectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal((int)HttpStatusCode.OK, okObjectResult.StatusCode);
            var returnValue = Assert.IsType<NewImageLabelDTO>(okObjectResult.Value);

            var expectedResult = new NewImageLabelDTO() {
                Id = _sampleLabel.Id,
                ImageUrl = _sampleLabel.Image.Url
            };
            Assert.Equal(expectedResult.Id, returnValue.Id);
            Assert.Equal(expectedResult.ImageUrl, returnValue.ImageUrl);

        }

    }
}
