﻿using Label.Api.Database;
using Label.Api.IRepository;
using Label.Api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Repository
{
    public class ImageRepository:IImageRepository
    {
        private LabelContext _dbContext;

        public ImageRepository(LabelContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ImageLabel GetNextLabel(string Username)
        {
            if (String.IsNullOrWhiteSpace(Username))
            {
                return null;
            }

            var currentLabel = _dbContext.Set<ImageLabel>().Include(x => x.Image).Where(x => x.UserId == Username).Where(x => !x.IsCompleted).FirstOrDefault();

            if (currentLabel == null)
            {
                var imageLeastLabels = _dbContext.Set<Image>()
                                                  .Where(x => x.Labels.Where(y=>y.UserId == Username).Count() == 0)
                                                  .OrderBy(x => x.Labels.Count()).OrderBy(x =>x.Id).FirstOrDefault();
                if(imageLeastLabels == null)
                {
                    return null;
                }

                currentLabel = new ImageLabel()
                {
                    IsCompleted = false,
                    UserId = Username,
                    ImageId = imageLeastLabels.Id
                };

                _dbContext.Add<ImageLabel>(currentLabel);
                _dbContext.SaveChanges();
            }

            return currentLabel;
        }

        public void CompleteImageLabel(ImageLabel label)
        {
            var dbLabel = _dbContext.Set<ImageLabel>().Where(x => x.Id == label.Id).First();

            dbLabel.IsCompleted = true;
            dbLabel.PictureType = label.PictureType;
            dbLabel.StyleFabricYards = label.StyleFabricYards;
            dbLabel.StyleTailorCost = label.StyleTailorCost;
            // dbLabel.UserId = label.UserId;
            dbLabel.FabricType = label.FabricType;
            dbLabel.EventStyle = label.EventStyle;
            dbLabel.AttireSingleMaleStylish = label.AttireSingleMaleStylish;
            dbLabel.AttireSingleMaleStyle = label.AttireSingleMaleStyle;
            dbLabel.AttireSingleMaleHasEmbroidery = label.AttireSingleMaleHasEmbroidery;
            dbLabel.AttireSingleGender = label.AttireSingleGender;
            dbLabel.AttireSingleFemaleStylish = label.AttireSingleFemaleStylish;
            dbLabel.AttireSingleFemaleStyle = label.AttireSingleFemaleStyle;
            dbLabel.AttireSingleBodyType = label.AttireSingleBodyType;
            dbLabel.AttireSingleAgeGroup = label.AttireSingleAgeGroup;
            dbLabel.AttirePeople = label.AttirePeople;
            dbLabel.AttireGroupSameFabric = label.AttireGroupSameFabric;
            dbLabel.AttireGroupPeople = label.AttireGroupPeople;
            dbLabel.AttireGroupParentChildRelationship = label.AttireGroupParentChildRelationship;
            dbLabel.AttireGroupFamily = label.AttireGroupFamily;
            dbLabel.AttireGroupDescription = label.AttireGroupDescription;
            dbLabel.AttireFabricYards = label.AttireFabricYards;
            dbLabel.AttireFabricType = label.AttireFabricType;
            dbLabel.AccessoryType = label.AccessoryType;
            dbLabel.AccessoryFabricYards = label.AccessoryFabricYards;
            dbLabel.AccessoryFabricType = label.AccessoryFabricType;

            _dbContext.Set<ImageLabel>().Update(dbLabel);
            _dbContext.SaveChanges();
        }
    }
}
