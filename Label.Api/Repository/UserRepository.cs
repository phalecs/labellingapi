using Label.Api.IRepository;
using Label.Api.Logic;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public UserRepository(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }
        public string GetUserId()
        {
            if (_contextAccessor.HttpContext.User.HasClaim(x => x.Type == "user_id"))
            {
                return _contextAccessor.HttpContext.User.Claims.First(x => x.Type == "user_id").Value;
            }

            return String.Empty;
        }
    }
}
