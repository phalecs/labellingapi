﻿using Label.Api.IRepository;
using Label.Api.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Repository
{
    public class OptionsRepository : IOptionsRepository
    {
        public IEnumerable<KeyValuePair<Int16, string>> GetEnumResponse<T>()
        {
            return EnumExtension.GetEnumDictionary<T>().ToList();
        }
    }
}
