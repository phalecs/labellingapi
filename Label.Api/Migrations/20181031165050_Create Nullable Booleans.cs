﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Label.Api.Migrations
{
    public partial class CreateNullableBooleans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "AttireSingleMaleHasEmbroidery",
                table: "ImageLabels",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "AttireGroupSameFabric",
                table: "ImageLabels",
                nullable: true,
                oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "AttireSingleMaleHasEmbroidery",
                table: "ImageLabels",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "AttireGroupSameFabric",
                table: "ImageLabels",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
