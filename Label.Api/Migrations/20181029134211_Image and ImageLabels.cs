﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Label.Api.Migrations
{
    public partial class ImageandImageLabels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ImageLabels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    EventStyle = table.Column<int>(nullable: true),
                    StyleTailorCost = table.Column<int>(nullable: true),
                    StyleFabricYards = table.Column<int>(nullable: true),
                    AttireSingleFemaleStyle = table.Column<int>(nullable: true),
                    AttireSingleFemaleStylish = table.Column<int>(nullable: true),
                    AttireSingleMaleStylish = table.Column<int>(nullable: true),
                    AttireSingleMaleHasEmbroidery = table.Column<bool>(nullable: false),
                    AttireSingleMaleStyle = table.Column<int>(nullable: true),
                    AttireGroupFamily = table.Column<int>(nullable: true),
                    AttireGroupParentChildRelationship = table.Column<int>(nullable: true),
                    AttireSingleBodyType = table.Column<int>(nullable: true),
                    AttireSingleGender = table.Column<int>(nullable: true),
                    AttireSingleAgeGroup = table.Column<int>(nullable: true),
                    AttireGroupSameFabric = table.Column<bool>(nullable: false),
                    AttireGroupPeople = table.Column<int>(nullable: true),
                    AttireGroupDescription = table.Column<int>(nullable: true),
                    AttireFabricYards = table.Column<int>(nullable: true),
                    AttirePeople = table.Column<int>(nullable: true),
                    AttireFabricType = table.Column<int>(nullable: true),
                    AccessoryFabricYards = table.Column<int>(nullable: true),
                    AccessoryFabricType = table.Column<int>(nullable: true),
                    AccessoryType = table.Column<int>(nullable: true),
                    FabricType = table.Column<int>(nullable: true),
                    PictureType = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    ImageId = table.Column<int>(nullable: false),
                    IsCompleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageLabels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ImageLabels_Images_ImageId",
                        column: x => x.ImageId,
                        principalTable: "Images",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ImageLabels_ImageId",
                table: "ImageLabels",
                column: "ImageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImageLabels");

            migrationBuilder.DropTable(
                name: "Images");
        }
    }
}
