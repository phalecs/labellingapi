﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Label.Api.Migrations
{
    public partial class UniqueUrlsandImported : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Images_Url",
                table: "Images",
                column: "Url",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Images_Url",
                table: "Images");
        }
    }
}
