﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.ViewModels
{
    public class NewImageLabelDTO
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }

    }
}
