﻿using Label.Api.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models
{
    public class ImageLabel
    {
        public int Id { get; set; }
        public EventStyle? EventStyle { get; set; }
        public TailorCost? StyleTailorCost { get; set; }
        public FabricYard? StyleFabricYards { get; set; }
        public Style? AttireSingleFemaleStyle { get; set; }
        public Stylish? AttireSingleFemaleStylish { get; set; }
        public Stylish? AttireSingleMaleStylish { get; set; }
        public bool? AttireSingleMaleHasEmbroidery { get; set; }
        public Style? AttireSingleMaleStyle { get; set; }
        public FamilyType? AttireGroupFamily { get; set; }
        public ParentChildRelationship? AttireGroupParentChildRelationship { get; set; }
        public BodyType? AttireSingleBodyType { get; set; }
        public Sex? AttireSingleGender { get; set; }        
        public AgeGroup? AttireSingleAgeGroup { get; set; }
        public bool? AttireGroupSameFabric { get; set; }
        public int? AttireGroupPeople { get; set; }
        public GroupDescription? AttireGroupDescription { get; set; }
        public FabricYard? AttireFabricYards { get; set; }
        public PictureNumberType? AttirePeople { get; set; }
        public FabricType? AttireFabricType { get; set; }
        public FabricYard? AccessoryFabricYards { get; set; }
        public FabricType? AccessoryFabricType { get; set; }
        public AccessoryType? AccessoryType { get; set; }
        public FabricType? FabricType { get; set; }
        public PictureType? PictureType { get; set; }
        public string UserId { get; set; }
        public int ImageId { get; set; }
        public virtual Image Image { get; set; }
        public bool IsCompleted { get; set; }
    }
}
