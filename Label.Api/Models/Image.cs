﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public virtual IEnumerable<ImageLabel> Labels { get; set; }
    }
}
