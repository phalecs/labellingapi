﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum Stylish
    {
        Classy = 1,
        Regular = 2,
        Boring = 3,
        Crazy = 4,
    }
}
