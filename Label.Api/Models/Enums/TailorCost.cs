﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum TailorCost
    {
        [Description("0-1000")]
        C_0_1000 = 1,
        [Description("1000-2500")]
        C_1000_2500 = 2,
        [Description("2500-4000")]
        C_2500_4000 = 3,
        [Description("4000-6000")]
        C_4000_6000 = 4,
    }
}
