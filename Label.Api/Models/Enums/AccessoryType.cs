﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum AccessoryType
    {
        [Description("Bag")]
        Bag = 1,
        [Description("Shoes")]
        Shoes = 2,
        [Description("Earings")]
        Earings = 3,
        [Description("Pillow")]
        Pillow = 4,
        [Description("Book")]
        Book = 5,
        [Description("Chair")]
        Chair = 6,
        [Description("Curtain")]
        Curtain = 7,
        [Description("Turban")]
        Turban =8,
        [Description("Hand fan")]
        Hand_Fan = 9,
        [Description("Bangles")]
        Bangles = 10,
        [Description("Tie")]
        Tie = 11,
        [Description("Table cover")]
        Table_Cover = 12,
        [Description("Cap")]
        Cap = 13,
        [Description("Hair ruffles")]
        Hair_Ruffles = 14,
        [Description("Bedsheet")]
        Bedsheet = 15,
        [Description("Hijab")]
        Hijab = 16
    }
}
