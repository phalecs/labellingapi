﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum FabricYard
    {
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        [Description("7+")]
        Seven_Plus = 7
    }
}
