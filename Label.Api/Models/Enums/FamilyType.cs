﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum FamilyType
    {
        [Description("Nuclear Family")]
        Nuclear_Family = 1,
        [Description("Extended Family")]
        Extended_Family = 2,
        [Description("Siblings")]
        Siblings = 3,

    }
}
