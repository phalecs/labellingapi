﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum FabricType
    {
        [Description("Lace")]
        Lace = 1,
        [Description("Atiku")]
        Atiku = 2,
        [Description("Ankara")]
        Ankara = 3,
        [Description("Kente")]
        Kente = 4,
        [Description("George")]
        George = 5,
        [Description("Chiffon")]
        Chiffon =6,
        [Description("Dashiki")]
        Dashiki = 7,
        [Description("Aso oke")]
        Aso_Oke = 8,
        [Description("Tie and dye")]
        Tie_And_Dye = 9,
        [Description("Guinea")]
        Guinea = 10,
        [Description("Damask")]
        Damask = 11,
        [Description("Border chiffon")]
        Border_Chiffon = 12,
        [Description("Crepe")]
        Crepe = 13,
        [Description("Trouser material")]
        Trouser_Material = 14,
        [Description("Lycra")]
        Lycra = 15,
        [Description("Satin")]
        Satin = 16,
        [Description("Wedding lace")]
        Wedding_Lace = 17,
        [Description("Cotton")]
        Cotton = 18,
        [Description("Sequin")]
        Sequin = 19,
        [Description("Adire")]
        Adire = 20,
        [Description("Batik")]
        Batik = 21,
    }
}
