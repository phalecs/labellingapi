﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum Style
    {
        [Description("Agbada")]
        Agbada = 1,
        [Description("Shirt and Trouser")]
        Shirt_And_Trouser = 2,
        [Description("Shirt Only")]
        Shirt_Only = 3,
        [Description("Trouser Only")]
        Trouser_Only = 4,
        [Description("Kaftan")]
        Kaftan = 5,
        [Description("Senator")]
        Senator = 6,
        [Description("Suit")]
        Suit = 7,
        [Description("Esiki (Armless Agbada)")]
        Esiki = 8,
        [Description("Waist Coat")]
        Waist_Coat = 9,
        [Description("Fila")]
        Fila = 10,
        [Description("T-Shirt")]
        Tshirt = 11,
        [Description("Isiagu (Igbo men style)")]
        Isiagu = 12,
        [Description("Top and wrapper (South south men style)")]
        Top_And_Wrapper_South_South = 13,
    }
}
