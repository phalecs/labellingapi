﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum BodyType
    {
        [Description("Skinny")]
        Skinny = 1,
        [Description("Smallish")]
        Smallish = 2,
        [Description("Muscular and Stout")]
        Muscular_And_Stout = 3,
        [Description("Muscular and Tall")]
        Muscular_And_Tall = 4,
        [Description("Plus Size")]
        Plus_Size = 5,
        [Description("Average")]
        Average = 6,
    }
}
