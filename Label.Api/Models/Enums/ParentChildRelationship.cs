﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum ParentChildRelationship
    {
        [Description("Mother and Son")]
        Mother_And_Son = 1,
        [Description("Mother and Daugther")]
        Mother_And_Daughter = 2,
        [Description("Father and Son")]
        Father_And_Son = 3,
        [Description("Father and Daughter")]
        Father_And_Daughter = 4,
        [Description("Grand parent and Children")]
        Grandparent_And_Children = 5,
    }
}
