﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum AgeGroup
    {
        [Description("0-2")]
        A_0_2 = 1,
        [Description("2-9")]
        A_2_9= 2,
        [Description("10-14")]
        A_10_14 = 3,
        [Description("15-18")]
        A_15_18 = 4,
        [Description("19-25")]
        A_19_25 = 5,
        [Description("26-35")]
        A_26_35 = 6,
        [Description("36-45")]
        A_36_45 = 7,
        [Description("46-60")]
        A_46_60 = 8,
        [Description("61-80")]
        A_61_80 = 9,
        [Description("81+")]
        A_81_Above = 10,
    }
}
