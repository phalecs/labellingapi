﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum PictureType
    {
        [Description("Fabric")]
        Fabric = 1,
        [Description("Attire/ Person/People in attire")]
        People_In_Attire = 2,
        [Description("Accessories")]
        Accesories = 3
    }
}
