﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum GroupDescription
    {
        [Description("A Couple(Husband and wife)")]
        Couple = 1,
        [Description("Parent and child")]
        Parent_And_Child = 2,
        [Description("A Family")]
        Family = 3,
        [Description("A group of friends")]
        Group_Of_Friends = 4,

    }
}
