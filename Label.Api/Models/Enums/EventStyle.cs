﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum EventStyle
    {
        [Description("Wedding guest")]
        Wedding = 1,
        [Description("Wedding(bride)")]
        Wedding_Bride = 2,
        [Description("Wedding(groom)")]
        Wedding_Groom =3,
        [Description("Wedding(bridal train)")]
        Wedding_Bridal_Train = 4,
        [Description("Wedding(groom's man)")]
        Wedding_Grooms_Man = 5,
        [Description("Muslim")]
        Muslim = 6,
        [Description("Burial")]
        Burial = 7,
        [Description("Casual")]
        Casual = 8,
        [Description("Red carpet")]
        Red_Carpet = 9,
        [Description("Church")]
        Church = 10,
        [Description("Executive")]
        Executive = 11,
        [Description("Work(friday)")]
        Work_Friday = 12,
        [Description("Family Potrait")]
        Family_Potrait = 13,

    }
}
