﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum StyleType
    {
        [Description("Short Skirt and blouse")]
        Short_Skirt_And_Blouse = 1,
        [Description("Long Skirt and blouse")]
        Long_Skirt_And_Blouse = 2,
        [Description("Short gown")]
        Short_Gown = 3,
        [Description("Long gown")]
        Long_Gown = 4,
        [Description("Off shoulder")]
        Off_Shoulder = 5,
        [Description("Iro and buba")]
        Iro_And_Buba = 6,
        [Description("Blouse and wrapper")]
        Blouse_And_Wrapper = 7,
        [Description("Skirt only)")]
        Skirt_Only = 8,
        [Description("Blouse only")]
        Blouse_Only = 9,
        [Description("Trouser only")]
        Trouser_Only = 10,
        [Description("Boubou")]
        Boubou = 11,
        [Description("Shirt and trouser")]
        Shirt_And_Trouser = 12,
        [Description("Benin traditional")]
        Benin_Traditional = 13,
        [Description("Calabar traditional")]
        Calabar_Traditional = 14,
        [Description("Jumpsuit")]
        Jumpsuit = 15,
        [Description("Female agbada")]
        Female_Agbada = 16,
        [Description("Maternity gown")]
        Maternity_Gown = 17,
    }
}
