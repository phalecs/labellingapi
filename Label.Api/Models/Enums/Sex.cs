﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum Sex
    {
        Male = 1,
        Female = 2
    }
}
