﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Models.Enums
{
    public enum PictureNumberType
    {
        [Description("No One")]
        No_One = 3,
        [Description("One person")]
        One_Person = 1,
        [Description("A group of people")]
        Group_Of_People = 2,
    }
}
