﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Label.Api.IRepository;
using Label.Api.Logic;
using Label.Api.Models.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Label.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OptionsController : ControllerBase
    {
        private readonly IOptionsRepository _optionsRepository;

        public OptionsController(IOptionsRepository optionsRepository)
        {
            _optionsRepository = optionsRepository;
        }
        // GET api/values

        [HttpGet]
        [Route("AccessoryType")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetAccessoryTypes()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<AccessoryType>());
        }
        
        [HttpGet]
        [Route("AgeGroup")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetAgeGroups()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<AgeGroup>());
        }

        [HttpGet]
        [Route("BodyType")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetBodyTypes()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<BodyType>());
        }

        [HttpGet]
        [Route("EventStyle")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetEventStyles()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<EventStyle>());
        }

        [HttpGet]
        [Route("FabricType")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetFabricTypes()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<FabricType>());
        }

        [HttpGet]
        [Route("FabricYard")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetFabricYards()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<FabricYard>());
        }

        [HttpGet]
        [Route("FamilyType")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetFamilyTypes()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<FamilyType>());
        }

        [HttpGet]
        [Route("GroupDescription")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetGroupDescriptions()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<GroupDescription>());
        }

        [HttpGet]
        [Route("ParentChildRelationship")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetParentChildrenRelationships()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<ParentChildRelationship>());
        }

        [HttpGet]
        [Route("PictureNumberType")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetPictureNumberTypes()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<PictureNumberType>());
        }

        [HttpGet]
        [Route("PictureType")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetPictureTypes()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<PictureType>());
        }

        [HttpGet]
        [Route("Sex")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetSexes()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<Sex>());
        }
        
        [HttpGet]
        [Route("Style")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetStyles()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<Style>());
        }

        [HttpGet]
        [Route("StyleType")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetStyleType()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<StyleType>());
        }

        [HttpGet]
        [Route("Stylish")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetStylish()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<Stylish>());
        }

        [HttpGet]
        [Route("TailorCost")]
        public async Task<IEnumerable<KeyValuePair<short, string>>> GetTailorCosts()
        {
            return await Task.FromResult(_optionsRepository.GetEnumResponse<TailorCost>());
        }
    }
}