﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Label.Api.IRepository;
using Label.Api.Models;
using Label.Api.Repository;
using Label.Api.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Label.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ImageLabelsController : ControllerBase
    {
        private IImageRepository _imageRepository;
        private IUserRepository _userRepository;

        public ImageLabelsController(IImageRepository imageRepository,
                                     IUserRepository userRepository)
        {
            _imageRepository = imageRepository;
            _userRepository = userRepository;
        }

        [Route("Next")]
        public IActionResult Get()
        {
            var UserId = _userRepository.GetUserId();
            var imageLabel = _imageRepository.GetNextLabel(UserId);
            
            if (imageLabel == null)
            {
                return NotFound();
            }

            var result = new NewImageLabelDTO()
            {
                Id = imageLabel.Id,
                ImageUrl = imageLabel.Image.Url,
            };

            return Ok(result);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] ImageLabel label)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _imageRepository.CompleteImageLabel(label);
            return Ok(label);
        }
    }
}
