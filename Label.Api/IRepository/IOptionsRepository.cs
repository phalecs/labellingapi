﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.IRepository
{
    public interface IOptionsRepository
    {
        IEnumerable<KeyValuePair<Int16, string>> GetEnumResponse<T>();
    }
}
