using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.IRepository
{
    public interface IUserRepository
    {
        String GetUserId();
    }
}
