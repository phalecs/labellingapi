﻿using Label.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.IRepository
{
    public interface IImageRepository
    {
        void CompleteImageLabel(ImageLabel label);

        ImageLabel GetNextLabel(string Username);
    }
}
