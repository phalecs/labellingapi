﻿using Label.Api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Database
{
    public class LabelContext: DbContext
    {
        public LabelContext(DbContextOptions options):base(options)
        {
             
        }

        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<ImageLabel> ImageLabels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Image>()
                .HasIndex(b => b.Url)
                .IsUnique();
        }
    }
}
