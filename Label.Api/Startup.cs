﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Label.Api.Database;
using Npgsql.EntityFrameworkCore.PostgreSQL.Infrastructure;
using Label.Api.IRepository;
using Label.Api.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Label.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            _configuration = LoadConfigurations(env);
        }

        public IConfiguration _configuration { get; }

        public virtual IConfiguration LoadConfigurations(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath);
            builder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            builder.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange:true);

            builder.AddYamlFile("config.yaml", optional: true, reloadOnChange:true);
            builder.AddYamlFile($"config.{env.EnvironmentName}.yaml", optional: true, reloadOnChange:true);

            return builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddCors();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    string authority = _configuration.GetValue<String>("JWT:Authority");
                    string audience = _configuration.GetValue<String>("JWT:Audience");
                    options.Authority = authority;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = authority,
                        ValidateAudience = true,
                        ValidAudience = audience,
                        ValidateLifetime = true
                    };
                });

            ConfigureDatabaseService(services);
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IImageRepository, ImageRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddSingleton<IOptionsRepository, OptionsRepository>();
           
        }

        public virtual void ConfigureDatabaseService(IServiceCollection services)
        {
            services.AddDbContextPool<LabelContext>(options =>
            {
                options.UseNpgsql(_configuration.GetConnectionString("DefaultConnection"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                              IHostingEnvironment env,
                              LabelContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // app.UseHsts();
            }

            app.UseAuthentication();

            if (!dbContext.Database.IsInMemory())
            {
                dbContext.Database.Migrate();
            }

            app.UseCors(builder => {
                builder.WithOrigins("*").AllowAnyHeader().AllowAnyMethod();
            });

            // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
