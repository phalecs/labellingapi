﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Label.Api.Logic
{
    public static class EnumExtension
    {
        public static IDictionary<Int16, string> GetEnumDictionary<T>()
        {
            var names = Enum.GetNames(typeof(T)).ToList();
            var theEnum = Enum.GetValues(typeof(T)).OfType<T>();
            var values = theEnum.Select(x => Convert.ToInt16(x)).ToList();
            var descriptions = theEnum.Select(x => GetDescription((IConvertible)x)).ToList();

            IEnumerable<Tuple<string, Int16, string>> result = names.Zip(values, (n, v) => new { n, v })
                                                                     .Zip(descriptions, (z1, d) => Tuple.Create(z1.n, z1.v, d));

            IDictionary<Int16, string> dict_result = new Dictionary<Int16, string>();
            foreach (var item in result)
            {
                dict_result.Add(item.Item2, item.Item3 ?? item.Item1);
            }

            return dict_result;
        }

        public static string GetDescription<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(DescriptionAttribute), false)
                            .FirstOrDefault() as DescriptionAttribute;

                        if (descriptionAttribute != null)
                        {
                            return descriptionAttribute.Description;
                        }
                    }
                }
            }

            return null; // could also return string.Empty
        }
    }
}
